---
author: Franck Chambon
title: Exercice 2 - Échange de deux valeurs
tags:
  - 0-simple
---
# Exercice 2 - Échange de deux valeurs

Écrire un script qui échange les valeurs contenues dans deux variables `a` et `b` :

{{ IDE('exo', affiche_correction=False) }}
