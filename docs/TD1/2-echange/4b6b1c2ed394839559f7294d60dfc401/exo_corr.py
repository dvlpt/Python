# Exemple pour a contenant 10 et b contenant 3,
# mais le script doit fonctionner avec n'importe quelles valeurs.
a = 10
b = 3

# Il faut passer par une troisième variable (temporaire)
# afin de stocker la valeur de a ...
c = a
# ... avant de l'écraser par celle de b.
a = b
b = c

# Tests

assert a == 3
assert b == 10
