# TD1 - Exercice 2

## Commentaires

Il est possible de tester la solution.

{{ IDE('exo_corr') }}

???+note "Remarque"

    Avec Python, il est possible de ne pas utiliser cette variable temporaire intermédiaire.
    
    En effet, grace à l'affectation multiple, il est possible d'écrire
    
    ```py
    # Exemple pour a contenant 10 et b contenant 3,
    # mais le script doit fonctionner avec n'importe quelles valeurs.
    a = 10
    b = 3
    
    a, b = b, a
    
    # Tests

    assert a == 3
    assert b == 10
    ```
    
    L'échange est alors effectué.
    
    En pratique, on preferera utiliser cette méthode, beaucoup plus simple !!