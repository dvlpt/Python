# Exemple pour a contenant 10 et b contenant 3,
# mais le script doit fonctionner avec n'importe quelles valeurs.
a = 10
b = 3

...

# Tests

assert a == 3
assert b == 10