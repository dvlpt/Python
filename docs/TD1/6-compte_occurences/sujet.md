---
author: Franck Chambon
title: Exercice 6 - Compte d'occurrences
tags:
  - 1-boucle
  - 2-string
---

# Exercice 6 - Occurrences d'un caractère dans un mot

Écrire une fonction `compte_occurrences` qui prend en paramètres `cible`, un caractère (**une chaine de caractères de longueur 1**), et `mot`, une chaine de caractères, et qui renvoie le nombre d'occurrences de `cible` dans `mot` ; c'est-à-dire le nombre de fois où `cible` apparait dans `mot`.

???+warning "Attention"
    On n'utilisera pas la méthode `count`.

!!! example "Exemples"

    ```pycon
    >>> compte_occurrences("o", "bonjour")
    2
    >>> compte_occurrences("a", "abracadabra")
    5
    >>> compte_occurrences("i", "abracadabra")
    0
    ```

{{ IDE('exo', SANS="count", affiche_correction=False) }}
