# TD1 - Exercice 5

## Commentaires

{{ IDE('exo_corr') }}

La fonction fait un arrêt prématuré et renvoie l'indice `i` dès que la valeur est découverte.

Si la valeur n'est pas trouvée, la fonction renvoie `None` comme toute fonction Python qui termine sans rencontrer de `return`.

Il n'est donc pas nécessaire de rajouter un `#!python return None` à la fin de la fonction, mais c'est possible ! La fonction suivante est également une solution :

```py
def indice(element, tableau):
    for i in range(len(tableau)):
        if tableau[i] == element:
            return i
    return None
```