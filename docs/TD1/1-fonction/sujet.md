---
author: Benoît Domart
title: Exercice 1 - Écrire une fonction
tags:
  - 0-simple
---
# Exercice 1 - Écrire une fonction

## 1. Somme de deux nombres

Compléter la fonction ci-dessous pour qu'elle renvoie la somme des deux arguments `a` et `b`.

???+ example "Exemple"

    ```pycon
    >>> somme(10, 32)
    42
    >>> somme(100, 7)
    107
    ```

{{ IDE('question1', affiche_correction=False) }}

## 2. Double d'un nombre

Compléter la fonction ci-dessous pour qu'elle renvoie le double du nombre en argument.

???+warning "Attention"
    Vous ne devez pas effectuer d'addition ou de multiplication ici, il faut utiliser **uniquement** la fonction `somme` écrite précédemment.

{{ IDE('question2', affiche_correction=False) }}


## 3. Quadruple d'un nombre

Compléter la fonction ci-dessous pour qu'elle renvoie le quadruple du nombre en argument.

???+warning "Attention"
    Vous ne devez pas effectuer d'addition ou de multiplication ici, il faut utiliser **uniquement** la fonction `double` écrite précédemment.

{{ IDE('question3', affiche_correction=False) }}
