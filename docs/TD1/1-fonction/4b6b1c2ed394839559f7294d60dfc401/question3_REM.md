# TD1 - Exercice 1 - Question 3

## Commentaires

???+warning "Attention !"

    Ici, il est demandé d'utiliser la fonction `double`.

Il est possible de tester la solution.

{{ IDE('question3_corr') }}
