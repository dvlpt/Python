def plus_grand_parmi_n():
    ...

# tests

assert plus_grand_parmi_n([-5]) == -5
assert plus_grand_parmi_n([1, 2, 3, -5, 1]) == 3
assert plus_grand_parmi_n([]) == None
