# -*- coding: utf-8 -*-

def plus_grand_parmi_2(a, b):
    # Si a est plus grand que b,
    if a >= b:
        # alors c'est a le plus grand des 2,
        return a
    else:
        # sinon c'est b.
        return b

assert plus_grand_parmi_2(1, 2) == 2
assert plus_grand_parmi_2(-1, -2) == -1
