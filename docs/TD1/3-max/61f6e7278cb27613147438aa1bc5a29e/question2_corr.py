# -*- coding: utf-8 -*-

def plus_grand_parmi_3(a, b, c):
    if a >= b and a >= c:
        # a est plus grand que b ET c : c'est le plus grand.
        return a
    elif b >= c:
        # On sait que ce n'est pas a le plus grand.
        # Comme b est plus grand que c, c'est b le plus grand.
        return b
    else:
        # On sait que ce n'est pas a le plus grand.
        # Comme b n'est pas plus grand que c, c'est c le plus grand.
        return c

assert plus_grand_parmi_3(1, 2, 3) == 3
assert plus_grand_parmi_3(-1, 2, -3) == 2
assert plus_grand_parmi_3(-1, -2, -3) == -1


# On peut aussi coder cette fonction en utilisant la fonction de la question
# précédente, et le fait que
# max(a, b, c) = max(max(a, b), c)
def plus_grand_parmi_3_version_2(a, b, c):
    d = plus_grand_parmi_2(a, b)
    return plus_grand_parmi_2(c, d)
