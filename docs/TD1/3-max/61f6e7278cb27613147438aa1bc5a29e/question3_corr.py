# -*- coding: utf-8 -*-

def plus_grand_parmi_n(liste):
    if len(liste) == 0:
        # Si la liste est vide, on retourne None.
        return None
    # Si on est ici, c'est que la liste n'est pas vide.
    # On peut donc prendre son 1er élément :
    plus_grand = liste[0]
    # On parcourt la liste, en comparant l'élément courant avec la
    # valeur sauvegardée dans "plus_grand".
    for element in liste:
        if element >= plus_grand:
            # Si on trouve un élément plus grand, on sauvegarde sa valeur
            # dans "plus_grand".
            plus_grand = element
    return plus_grand

assert plus_grand_parmi_n([-5]) == -5
assert plus_grand_parmi_n([1, 2, 3, -5, 1]) == 3
assert plus_grand_parmi_n([]) == None
