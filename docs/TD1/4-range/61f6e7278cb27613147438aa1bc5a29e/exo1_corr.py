# UN SEUL PARAMETRE #

# Les entiers de 0 (inclus) à 11 (exclu)
liste_1 = list(range(11))

# Les entiers de 0 (inclus) à 20 (exclu)
liste_2 = list(range(20))

# Les entiers de 0 (inclus) à 20 (inclus)
liste_3 = list(range(21))