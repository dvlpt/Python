# DEUX PARAMETRES #

# Les entiers de 3 (inclus) à 10 (exlu)
liste_4 = list(range(3, 10))

# Les entiers de 12 (inclus) à 20 (inclus)
liste_5 = list(range(12, 21))

# Les entiers de -10 (inclus) à 1 (exlu)
liste_6 = list(range(-10, 1))

# Les entiers de -10 (inclus) à 1 (inclus)
liste_7 = list(range(-10, 2))