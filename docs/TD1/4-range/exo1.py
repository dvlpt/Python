# UN SEUL PARAMETRE #

# Les entiers de 0 (inclus) à 11 (exclu)
liste_1 = list(range(...))

# Les entiers de 0 (inclus) à 20 (exclu)
liste_2 = list(range(...))

# Les entiers de 0 (inclus) à 20 (inclus)
liste_3 = list(range(...))

# Tests

assert liste_1 == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
assert liste_2 == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
assert liste_3 == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]