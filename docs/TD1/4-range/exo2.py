# DEUX PARAMETRES #

# Les entiers de 3 (inclus) à 10 (exlu)
liste_4 = list(range(..., ...))

# Les entiers de 12 (inclus) à 20 (inclus)
liste_5 = list(range(..., ...))

# Les entiers de -10 (inclus) à 1 (exlu)
liste_6 = list(range(..., ...))

# Les entiers de -10 (inclus) à 1 (inclus)
liste_7 = list(range(..., ...))

# Tests

assert liste_4 == [3, 4, 5, 6, 7, 8, 9]
assert liste_5 == [12, 13, 14, 15, 16, 17, 18, 19, 20]
assert liste_6 == [-10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0]
assert liste_7 == [-10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1]