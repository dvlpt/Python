def persistance_multiplicative(n):
    ...

# Tests

assert persistance_multiplicative(243) == 2
assert persistance_multiplicative(1) == 0
assert persistance_multiplicative(11) == 1
assert persistance_multiplicative(73) == 2
assert persistance_multiplicative(679) == 5
assert persistance_multiplicative(6788) == 6
assert persistance_multiplicative(68889) == 7
assert persistance_multiplicative(277777788888899) == 11