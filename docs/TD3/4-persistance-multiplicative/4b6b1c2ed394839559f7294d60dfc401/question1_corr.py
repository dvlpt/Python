def multiplication_chiffre(chaine):
    '''
    Cette sous méthode renvoie le produit des
    chiffres composants le nombre
    en paramètre
    '''
    result = 1
    for chiffre in chaine:
        # Le chiffre est une chaîne de caractères.
        # Il faut le "caster" en int.
        result *= int(chiffre)
    return result


def persistance_multiplicative(n):
    chaine = str(n)
    result = 0
    while len(chaine) > 1:
        result += 1
        chaine = str(multiplication_chiffre(chaine))
    return result
        
    
# Tests

assert persistance_multiplicative(243) == 2
assert persistance_multiplicative(1) == 0
assert persistance_multiplicative(11) == 1
assert persistance_multiplicative(73) == 2
assert persistance_multiplicative(679) == 5
assert persistance_multiplicative(6788) == 6
assert persistance_multiplicative(68889) == 7
assert persistance_multiplicative(277777788888899) == 11