# TD3 - Exercice 4 - Question 1

## Commentaires

Ici, il peut être pratique (mais pas obligatoire) de créer une fonction intermédiaire, `multiplication_chiffre`, qui renvoit le produit des chiffres d'un nombre donné.

{{ IDE('question1_corr') }}