def inverse_persistance_multiplicative(n):
    result = 0
    while persistance_multiplicative(result) < n:
        result += 1
    return result
        
    
# Tests

assert inverse_persistance_multiplicative(0) == 0
assert inverse_persistance_multiplicative(1) == 10
assert inverse_persistance_multiplicative(2) == 25
assert inverse_persistance_multiplicative(3) == 39
assert inverse_persistance_multiplicative(7) == 68889