---
author: Benoît Domart
title: Exercice 4 - Persistance multiplicative
tags:
  - 1-condition
  - 2-while
  - 3-maths
---
# Exercice 4 - Persistance multiplicative

Partons d'un nombre entier par exemple 243.

On fait le produit de ses chiffres : 2 × 4 × 3 = 24.

On fait de même pour le résultat obtenu : 2 × 4 = 8 et on ne peut pas continuer plus longtemps, car on a obtenu un nombre à un seul chiffre.

On dit que la [persistance multiplicative](https://fr.wikipedia.org/wiki/Persistance_d%27un_nombre){target=_blank} de 243 est 2.

???+hint "Indication"

    Pour extraire les différents chiffres d’un nombre, il peut être intéressant de le voir comme une chaîne de caractères.
    
    Il peut donc être intéressant d'effectuer du **transtypage** (du _cast_ en anglais).

## 1. Déterminer la persistance multiplicative

Compléter la fonction `persistance_multiplicative` qui permet de déterminer la persistance multiplicative de l'entier en paramètre.

{{ IDE('question1', affiche_correction=False) }}

## 2. Plus petit entier

Compléter la fonction `inverse_persistance_multiplicative` renvoyant le plus petit entier dont la persistance multiplicative est au moins l'entier `n` en paramètre.

{{ IDE('question2', affiche_correction=False) }}