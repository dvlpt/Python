---
author: Benoît Domart
title: Exercice 3 - Algorithme d'Euclide
tags:
  - 1-condition
  - 2-while
---
# Exercice 3 - Algorithme d'Euclide

Compléter la fonction `euclide` pour qu'elle renvoie le pgcd des deux entiers positifs en paramètre.

???+tip "L'algorithme d'Euclide"
    Pour rappel, voici une version de cet algorithme :
    
    ``` title="L'algorithme d'Euclide"
    Tant que b est différent de 0,
        Calculer r le reste de la division euclidienne de a par b.
        a devient b.
        b devient r.
    Renvoyer a.
    ```

En Python, l'opérateur `%` permet d'obtenir le reste de la division euclidienne :

???+example "Exemples"

    ```pycon
    >>> 2%2
    0
    >>> 3%2
    1
    >>> 4%2
    0
    >>> 5%2
    1
    >>> 18%5
    3
    ```

{{ IDE('question1', affiche_correction=False) }}