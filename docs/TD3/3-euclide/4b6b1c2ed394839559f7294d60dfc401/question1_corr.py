def euclide(a, b):
    while b != 0:
        a, b = b, a%b
    return a
    
# Tests

assert euclide(10, 1) == 1
assert euclide(10, 6) == 2
assert euclide(10, 5) == 5
assert euclide(10, 7) == 1
assert euclide(221, 782) == 17
assert euclide(223, 782) == 1