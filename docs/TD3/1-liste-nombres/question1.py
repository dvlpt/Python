def extremums(liste):
    ...

# Tests

assert extremums([]) == ()
assert extremums([5]) == (5, 5)
assert extremums([2, -4.3, -5, 2, 7]) == (-5, 7)