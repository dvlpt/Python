def moyenne(liste):
    ...

# Tests

assert moyenne([]) == 0
assert moyenne([5]) == 5
assert moyenne([2, -4, -5, 2, 7]) == 0.4