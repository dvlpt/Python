---
author: Benoît Domart
title: Exercice 1 - Liste de nombres
tags:
  - 1-boucle
  - 2-liste
---
# Exercice 1 - Liste de nombres

## 1. Extremums

Compléter la fonction `extremums` pour qu'elle renvoie la plus petite et la plus grande valeur de la liste de nombres en paramètre.

???+warning "Attention"
    Il ne faut pas utiliser les fonctions `max` et `min`.

{{ IDE('question1', SANS='max, min', affiche_correction=False) }}

## 2. Somme

Compléter la fonction `somme` pour qu'elle renvoie la somme des éléments de la liste de nombres en paramètre.

???+warning "Attention"
    Il ne faut pas utiliser la fonction `sum`.

{{ IDE('question2', SANS='sum', affiche_correction=False) }}

## 3. Moyenne

Compléter la fonction `moyenne` pour qu'elle renvoie la moyenne des éléments de la liste de nombres en paramètre.

???+warning "Attention"
    Il ne faut pas utiliser la fonction `sum`.

{{ IDE('question3', SANS='sum', affiche_correction=False) }}