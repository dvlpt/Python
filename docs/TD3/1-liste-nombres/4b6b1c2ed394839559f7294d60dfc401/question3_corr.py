def moyenne(liste):
    if len(liste) == 0:
        return 0
    return somme(liste) / len(liste)

# Tests

assert moyenne([]) == 0
assert moyenne([5]) == 5
assert moyenne([2, -4, -5, 2, 7]) == 0.4