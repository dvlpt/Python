def extremums(liste):
    if len(liste) == 0:
        return ()
    M = liste[0]
    m = liste[0]
    for element in liste:
        if element > M:
            M = element
        if element < m:
            m = element
    return m, M

# Tests

assert extremums([]) == ()
assert extremums([5]) == (5, 5)
assert extremums([2, -4.3, -5, 2, 7]) == (-5, 7)