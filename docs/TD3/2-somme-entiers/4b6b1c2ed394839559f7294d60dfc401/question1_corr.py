def question_1(n):
    for i in range(1, n+1):
        # i va de 1 à n.
        # À chaque tour de boucle, on affiche sa valeur.
        print(i)

def somme_de_1_a_n(n):
    # Pour calculer la sommes des n premiers entiers, on initialise une variable somme à 0.
    somme = 0
    for i in range(1, n+1):
        # À chaque tour de boucle, on augmente la valeur contenue dans cette variable somme de l'entier i.
        somme += i # Cette instruction est équivalente à "somme = somme + i".
    return somme

def somme_de_1_a_n_bis(n):
    '''
    Une correction avec la fonction sum (interdite ici).
    '''
    return sum([i for i in range(n+1)])
    
# Tests

assert somme_de_1_a_n(10) == 55
assert somme_de_1_a_n(20) == 210
assert somme_de_1_a_n(100) == 5050