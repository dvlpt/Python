---
author: Benoît Domart
title: Exercice 2 - Somme des n premiers entiers
tags:
  - 1-boucle
---
# Exercice 2 - Somme des n premiers entiers

Compléter la fonction `somme_de_1_a_n` pour qu'elle renvoie la somme des `n` premiers entiers, où `n` est le paramètre.

???+warning "Attention"
    Il ne faut pas utiliser la fonction `sum`.

???hint "Aide"

    On peut procéder par étape :
    
    1. Créer la boucle for qui affiche les entiers de 1 à n.
    2. Créer une variable somme , initialisée à 0 , et qui augmente à chaque tour de boucle de l'entier affiché.

{{ IDE('question1', SANS='sum', affiche_correction=False) }}