def eratosthene(n):
    ...
    
# Tests

assert eratosthene(2) == [False, False]
assert eratosthene(3) == [False, False, True]
assert eratosthene(4) == [False, False, True, True]
assert eratosthene(5) == [False, False, True, True, False]
assert eratosthene(6) == [False, False, True, True, False, True]
assert eratosthene(7) == [False, False, True, True, False, True, False]
assert eratosthene(23) == [False, False, True, True, False, True, False, True, False, False, False, True, False, True, False, False, False, True, False, True, False, False, False]