def liste_nombres_premiers(debut, fin):
    ...
    
# Tests

assert liste_nombres_premiers(0, 2) == []
assert liste_nombres_premiers(0, 4) == [2, 3]
assert liste_nombres_premiers(0, 10) == [2, 3, 5, 7]
assert liste_nombres_premiers(15, 20) == [17, 19]
assert liste_nombres_premiers(17, 23) == [17, 19]