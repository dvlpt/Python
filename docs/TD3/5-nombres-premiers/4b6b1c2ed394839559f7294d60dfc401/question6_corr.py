def conjecture_goldbach(n):
    resultat = []
    for i in liste_nombres_premiers(2, n):
        for j in liste_nombres_premiers(i, n):
            if i+j == n:
                resultat.append((i,j))
    return resultat
    
# Tests

assert conjecture_goldbach(4) == [(2, 2)]
assert conjecture_goldbach(6) == [(3, 3)]
assert conjecture_goldbach(8) == [(3, 5)]
assert conjecture_goldbach(10) == [(3, 7), (5, 5)]
assert conjecture_goldbach(50) == [(3, 47), (7, 43), (13, 37), (19, 31)]