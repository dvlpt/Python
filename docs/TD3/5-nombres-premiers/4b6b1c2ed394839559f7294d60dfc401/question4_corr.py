def liste_facteurs_premiers(n):
    result = []
    for i in range(2, n+1):
        if n%i == 0 and est_premier(i):
            result.append(i)
    return result
    
# Tests

assert liste_facteurs_premiers(2) == [2]
assert liste_facteurs_premiers(3) == [3]
assert liste_facteurs_premiers(4) == [2]
assert liste_facteurs_premiers(5) == [5]
assert liste_facteurs_premiers(6) == [2, 3]
assert liste_facteurs_premiers(7) == [7]
assert liste_facteurs_premiers(8) == [2]
assert liste_facteurs_premiers(9) == [3]
assert liste_facteurs_premiers(60) == [2, 3, 5]