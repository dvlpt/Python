# TD1 - Exercice 5 - Question 6

## Commentaires

Dans les deux boucles, il n'est pas nécessaire d'aller jusqu'à `n` inclus. En effet, `n` est un entier pair supérieur à 3, donc on est sûr qu'il n'est pas premier.

{{ IDE('question6_corr') }}

### Solution avec une seule boucle

Il est possible de résoudre ce problème avec une seule boucle. Dans ce cas, il faut faire attention à ne pas avoir de doublon.
Il faut donc vérifier, lors de l'ajout d'un couple, qu'il n'est pas déjà présent.
On utilise ici une liste en compréhension (qui n'est pas au programme ici) :

```py
(elem[0] for elem in resultat) # créé la liste des premiers éléments de chaques couples présents dans resultat.
```

```py
def conjecture_goldbach(n):
    resultat = []
    for i in range(2, n):
        if est_premier(i) and est_premier(n-i) and n-i not in (elem[0] for elem in resultat):
            resultat.append((i, n-i))
    return resultat
```