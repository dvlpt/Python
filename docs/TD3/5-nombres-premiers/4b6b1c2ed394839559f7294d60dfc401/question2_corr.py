def est_premier(n):
    if n < 2:
        return False
    for i in range(2, n):
        if n%i == 0:
            return False
    return True
    
# Tests

assert est_premier(2) == True
assert est_premier(3) == True
assert est_premier(4) == False
assert est_premier(5) == True
assert est_premier(6) == False
assert est_premier(7) == True
assert est_premier(15) == False
assert est_premier(23) == True