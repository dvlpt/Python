# Tests

assert est_premier(2) == True
assert est_premier(3) == True
assert est_premier(4) == False
assert est_premier(5) == True
assert est_premier(6) == False
assert est_premier(7) == True
assert est_premier(15) == False
assert est_premier(23) == True

# Autres tests

assert est_premier(0) == False
assert est_premier(1) == False