---
author: Benoît Domart
title: Exercice 5 - Les nombres premiers
tags:
  - 1-range
  - 1-condition
  - 2-while
  - 3-maths
---
# Exercice 5 - Les nombres premiers

## 1. Est-ce qu'un nombre est pair ?

Compléter la fonction `est_pair` qui renvoie `#!python True` si l'entier en paramètre est pair, et `#!python False` sinon.

???+note
    Pour rappel, un entier est pair s'il est divisible par 2, **c'est-à-dire si le reste de la division euclidienne de cet entier par 2 est égal à 0**.

{{ IDE('question1', affiche_correction=False) }}

## 2. Est-ce qu'un nombre est premier ?

Compléter la fonction `est_premier` qui renvoie `True` si l'entier en paramètre est premier, et `False` sinon.

???+note
    Pour rappel, un entier est premier s'il admet **exactement** deux diviseurs **distincts** : 1 **et** et lui-même.

{{ IDE('question2', affiche_correction=False) }}

## 3. Le crible d'Eratosthène

Compléter la fonction `eratosthene` qui renvoit un tableau de booléens de taille `n` (le paramètre), et qui indique, pour chaque nombre entre 0 et n-1 s'il est premier ou non.

Cette fonction doit appeler `est_premier`.

???+example "Exemple"

    ```pycon
    >>> eratosthene(6)
    [False, False, True, True, False, True]
    ```
    
    En effet, `eratosthene(6)` renvoit par exemple un tableau de taille 6 (c'est la valeur de `n`), qui concerne les entiers de 0 à 5 (de `0` à `n-1`) :
    
    - 0 n'est pas premier,
    - 1 n'est pas premier,
    - 2 est premier,
    - 3 est premier,
    - 4 n'est pas premier,
    - 5 est premier.

{{ IDE('question3', affiche_correction=False) }}


## 4. Liste des facteurs premiers

Tout entier positif supérieur ou égal à 2 peut être écrit comme un produit de nombre premier.

???+example "Exemples de décomposition en produit de facteurs premiers"

    - \(2 = 2\)
    - \(3 = 3\)
    - \(4 = 2\times{}2\)
    - \(5 = 5\)
    - \(6 = 2\times{}3\)
    - \(7 = 7\)
    - \(8 = 2\times{}2\times{}2\)
    - \(9 = 3\times{}3\)
    - ...
    - \(60 = 2\times{}2\times{}3\times{}5\)
    - ...

Compléter la fonction `liste_facteurs_premiers` qui prend un entier `n` supérieur ou égal à 2 en paramètre, et qui renvoie la liste de ses facteurs premiers.

Cette fonction doit appeler `est_premier`.

{{ IDE('question4', affiche_correction=False) }}

## 5. Liste des nombres premiers

Compléter la fonction `liste_nombres_premiers` qui renvoie un tableau contenant la liste des nombres premiers compris entre `debut` (inclus) et `fin` (exclus).

{{ IDE('question5', affiche_correction=False) }}

## 6. La conjecture de Goldbach

La conjecture de Goldbach est la suivante :

???+note "Conjecture de Goldbach"

    _Tout nombre entier pair supérieur à 3 peut s’écrire comme la somme de deux nombres premiers._

Compléter la fonction `conjecture_goldbach` prenant un entier `n` pair strictement supérieur à 2, et qui renvoie la liste des **couples de nombres premiers** dont la somme est `n`.

???+example "Exemples"

    ```pycon
	>>> conjecture_goldbach(4)
    [(2, 2)]
	>>> conjecture_goldbach(6)
    [(3, 3)]
	>>> conjecture_goldbach(8)
    [(3, 5)]
	>>> conjecture_goldbach(10)
    [(3, 7), (5, 5)]
	>>> conjecture_goldbach(50)
    [(3, 47), (7, 43), (13, 37), (19, 31)]
    ```
    
    Attention aux doublons, il ne faut pas que `conjecture_goldbach(8)` renvoie `[(3, 5), (5, 3)]` !

{{ IDE('question6', affiche_correction=False) }}

## 7. Le prochain nombre premier

Écrire une fonction Python prenant en paramètre un entier positif, et qui renvoie le plus petit nombre premier supérieur ou égal à cet entier.

???+example "Exemples"

    ```pycon
	>>> prochain_premier(0)
    2
	>>> prochain_premier(1)
    2
	>>> prochain_premier(2)
    2
	>>> prochain_premier(3)
    3
	>>> prochain_premier(10)
    11
	>>> prochain_premier(24)
    29
	>>> prochain_premier(32)
    37
    ```

{{ IDE('question7', affiche_correction=False) }}