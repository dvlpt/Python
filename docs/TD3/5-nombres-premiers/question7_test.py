# Tests

assert prochain_premier(0) == 2
assert prochain_premier(1) == 2
assert prochain_premier(2) == 2
assert prochain_premier(3) == 3
assert prochain_premier(10) == 11
assert prochain_premier(24) == 29
assert prochain_premier(32) == 37
assert prochain_premier(90) == 97