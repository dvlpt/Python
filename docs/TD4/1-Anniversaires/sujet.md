---
author: Nicolas Revéret
title: Exercice 1 - Anniversaires
tags:
  - 0-simple
  - 2-dictionnaire
---

# Exercice 1 - Anniversaires

On dispose d'un dictionnaire Python dans lequel :

* les clés sont les prénoms de différentes personnes. Il n'y a aucun prénom en double
* les valeurs sont les mois de naissance de ces personnes stockées sous forme de nombres entiers (`1` pour janvier, ... `12` pour décembre)

Par exemple :

```python
naissances = {'Nicolas': 10, 'Antoine': 7, 'Camille': 7}
```

Vous devez écrire une fonction `anniversaires(naissances, mois)` prenant en arguments un dictionnaire comme celui décrit ci-dessus (liste de couples nom/numéro de mois) ainsi qu'un numéro d'un mois et renvoyant une liste contenant les prénoms des personnes nées durant ce mois.

!!! example "Exemples"

    ```pycon
    >>> anniversaires({'Nicolas': 10, 'Antoine': 7, 'Camille': 7}, 1)
    []
    >>> anniversaires({'Nicolas': 10, 'Antoine': 7, 'Camille': 7}, 10)
    ['Nicolas']
    >>> anniversaires({'Nicolas': 10, 'Antoine': 7, 'Camille': 7}, 7)
    ['Antoine', Camille]
    >>> anniversaires({'Nicolas': 10, 'Antoine': 7, 'Camille': 7}, 13)
    []
    >>> anniversaires({}, 1)
    []
    ```

???+hint "Aide"

    Un dictionnaire Python est objet itérable. On peut donc écrire :

    ```py
    for elem in dictionnaire:
    ```

    Dans ce cas, `elem` va être successivement égal à chaque clef présente dans le dictionnaire.

    Ici, `elem` sera donc égal à `'Nicolas'`, puis `'Antoine'`, puis `'Camille'`, mais pas forcément dans cet ordre là ! **Un dictionnaire n'est pas une collection ordonnée.**

    Pour accéder aux valeurs qui correspondent à chaque fois aux clefs, on écrit :

    ```py
    dictionnaire[elem]
    ```

{{ IDE('exo', affiche_correction=False) }}
