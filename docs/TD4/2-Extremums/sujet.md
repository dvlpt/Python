---
author: Franck Chambon
title: Exercice 2 - Valeurs extrêmes
tags:
  - 0-simple
  - 2-dictionnaire
---

# Exercice 2 - Dictionnaire de valeurs extrêmes

???+note "Remarque"

    Il s'agit du même problème que [celui de la question 1 de l'exercice 1 du TD3](../../../TD3/1-liste-nombres/sujet){target=_blank}, mais la fonction ne doit pas renvoyer le résultat sous le même format (ici, il faut créer un dictionnaire).

Écrire une fonction `extremes` qui prend en paramètre une liste `valeurs` de nombres _a priori_ non triés, et qui renvoie la plus petite ainsi que la plus grande valeur du tableau sous la forme d'un dictionnaire à deux clés `'min'` et `'max'`.

???+note "Remarque"
    Si le tableau est vide, les extrêmes n'existent pas ; on utilisera alors `None` pour chacun.

???+warning "Attention"
    On n'utilisera pas les fonctions `min` et `max` fournies par le langage.

!!! example "Exemples"

    ```pycon
    >>> valeurs = [0, 1, 4, 2, -2, 9, 3, 1, 7, 1]
    >>> resultat = extremes(valeurs)
    >>> resultat
    {'min': -2, 'max': 9}
    ```

    ```pycon
    >>> valeurs = [37, 37]
    >>> resultat = extremes(valeurs)
    >>> resultat
    {'min': 37, 'max': 37}
    ```

    ```pycon
    >>> valeurs = []
    >>> resultat = extremes(valeurs)
    >>> resultat
    {'min': None, 'max': None}
    ```


{{ IDE('exo', SANS="min, max", affiche_correction=False) }}
