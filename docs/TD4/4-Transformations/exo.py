def transformation(motif, regles):
    ...


def n_transformations(motif, regles, n):
    ...


# Tests
regles = {'a': 'ab', 'b': 'ac', 'c': 'd'}
motif = 'a'
assert transformation(motif, regles) == 'ab'
assert n_transformations(motif, regles, 2) == 'abac'
assert n_transformations(motif, regles, 3) == 'abacabd'
assert n_transformations(motif, regles, 5) == 'abacabdabacdabacabdd'
assert n_transformations("rien ne change !", {'z': 'y'}, 50) == 'rien ne change !'
