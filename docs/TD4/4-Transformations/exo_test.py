# Tests
regles = {'a': 'ab', 'b': 'ac', 'c': 'd'}
motif = 'a'
assert transformation(motif, regles) == 'ab'
assert n_transformations(motif, regles, 2) == 'abac'
assert n_transformations(motif, regles, 3) == 'abacabd'
assert n_transformations(motif, regles, 5) == 'abacabdabacdabacabdd'
assert n_transformations("rien ne change !", {'z': 'y'}, 50) == 'rien ne change !'


# Tests supplémentaires
# Identité
regles = {'a': 'a', 'b': 'b'}
motif = 'ab'
assert transformation(motif, regles) == 'ab'
assert n_transformations(motif, regles, 20) == 'ab'

# Doublement
regles = {'a': 'aa'}
motif = 'a'
assert transformation(motif, regles) == 'aa'
assert n_transformations(motif, regles, 4) == 'aaaaaaaaaaaaaaaa'

# On échange
regles = {'a': 'b', 'b': 'a'}
motif = 'ac'
assert transformation(motif, regles) == 'bc'
assert n_transformations(motif, regles, 4) == 'ac'
assert n_transformations(motif, regles, 5) == 'bc'

# Pas de transformation -> cas limite
assert n_transformations(motif, regles, 0) == motif
