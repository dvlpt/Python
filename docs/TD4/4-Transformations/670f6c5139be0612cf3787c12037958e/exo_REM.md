## Commentaires

Le problème s'apparente aux [*L-systèmes*](https://fr.wikipedia.org/wiki/L-Syst%C3%A8me) inventés en 1968 par le biologiste Aristid Lindenmayer afin de décrire le développement d'êtres vivants.

## Une solution possible

{{ IDE('exo_corr') }}

La fonction `transformation` commence par créer la chaine vide `resultat`.

On parcourt ensuite le `motif`. Pour chaque `caractere` :

* si celui-ci fait partie des clés du dictionnaire `regles`, on ajoute la valeur associée à `resultat`,
  
* sinon, on le recopie à l'identique.

On termine en renvoyant `resultat`.

Dans la fonction `n_transformations`, on réitère la transformation en prenant soin de mettre à jour le `motif` avec le résultat de `transformation(motif, regles)`.

## Remarque

La concaténation de chaines de caractères (`resultat += regles[caractere]`) est "couteuse" en Python. Répéter cette action un grand nombre de fois peut allonger le temps d'exécution du code. Afin d'éviter ce désagrément, on préfère utiliser la méthode `#!py str.join`. Dans ce cas, le code devient :

```python
def transformation(motif, regles):
    resultats = []
    for caractere in motif:
        if caractere in regles:
            resultats.append(regles[caractere])
        else:
            resultats.append(caractere)
            
    return "".join(resultats)
```

## Solution fonctionnelle

On pourrait aussi raccourcir le test central en faisant :

```python
def transformation(motif, regles):
    resultats = []
    for caractere in motif:
        resultat.append(regles[caractere] if caractere in regles else caractere)
    return "".join(resultats)
```

En allant au bout de la démarche on obtiendrait un code *fonctionnel* en une ligne :

```python
def transformation(motif, regles):
    return "".join((regles[caractere] if caractere in regles else caractere for caractere in motif))
```
