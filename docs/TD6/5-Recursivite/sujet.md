author: Denis QUENTON
title: Boîte Noire

---

# Boîte Noire

Une machine un peu spéciale renvoie une suite de nombres Y, lorsqu'on lui entre une suite de nombres. Elle n'admet pas n'importe quelle suite de nombres en entrée.

Cette machine respecte les deux règles suivantes.

*   règle 1 : Si on entre la chaine "2"+ X, alors la machine donne la chaine X.
*   règle 2 : Si la machine donne Y quand on rentre X, alors elle donne Y + "2" +Y quand on entre "3" + X

De ces deux règles, on peut en déduire que les nombres en entrée ne peuvent commencer que par 2 ou 3, mais pas tous. 
De façon générale, les nombres acceptables en entrée sont les nombres de la forme : 2X, 32X, 332X, 3332X et ainsi de suite, où X est une suite de nombres quelconques non vide. Si le nombre n'est pas acceptable en entrée, la machine ne renvoie rien.

!!! example "Exemples"

    ```pycon
    >>> donne("2756")
    '756'
    >>> donne("2")
    >>> donne("3")
    ```

    ```pycon
    >>> donne("32756")
    '7562756'
    >>> donne("323")
    '323'
    ```
On demande de compléter le script suivant :

{{ IDE('exo') }}
