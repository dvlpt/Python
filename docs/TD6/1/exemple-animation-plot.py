import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as anim

def init():
    """
    Fonction appelée à l'initialisation de l'animation.
    """
    plt.xlim(0, 2*np.pi)
    plt.ylim(-1.2, 1.2)

def animate(numero_iteration):
    """
    Fonction gérant l'animation.
    x et y sont deux vecteurs de même taille, contenant respectivement les abscisses
    et les ordonnées de points à afficher.
    
    numero_iteration est le numéro de l'itération ...
    """
    # x est un vecteur de taille 150, entre 0 et 2pi.
    x = np.linspace(0, 2*np.pi, 150) # recalculé à chaque fois, ce qui est inutile.
    # y contient les images des valeurs de x par x -> cos(x+numero_iteration/10).
    y = np.cos(x+numero_iteration/10)
    line.set_data(x, y)
    return line,

vitesse = 1        # Permet de régler la vitesse de défilement.
fig = plt.figure() # Initialise la figure.
line, = plt.plot([], [])

# Lance l'animation :
ani = anim.FuncAnimation(fig, animate, init_func=init, interval=20/vitesse)
plt.show() # Affiche la fenêtre - Pas nécessaire avec Spyder, mais l'est avec IDLE.