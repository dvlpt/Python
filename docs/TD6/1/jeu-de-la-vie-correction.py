import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import formes

def init():
    """
    Initialise l'affichage de la fenêtre. Fonction à ne pas modifier !
    """
    plt.xlim(-1, N+1)
    plt.ylim(-1, N+1)
    return ln,

def nb_voisins_vivants(i, j):
    """
    Calcul le nombre de voisins vivants de la case (i, j).
    """
    ligne_avant = plateau_jeu[i-1,j-1] + plateau_jeu[i-1,j] + plateau_jeu[i-1,j+1]
    ligne = plateau_jeu[i,j-1] + plateau_jeu[i,j+1]
    ligne_apres = plateau_jeu[i+1,j-1] + plateau_jeu[i+1,j] + plateau_jeu[i+1,j+1]
    return ligne_avant + ligne + ligne_apres

def etape_suivante_jeu_de_la_vie():
    """
    Génère une nouvelle matrice, de la même taille que plateau_jeu,
    qui représente l'état du plateau à l'étape suivante.
    """
    plateau_jeu_suivant = np.zeros((N+2,N+2))
    for i in range(1,N+1):
        for j in range(1,N+1):
            nb_v = nb_voisins_vivants(i,j)
            if plateau_jeu[i,j] == 1:
                if nb_v == 2 or nb_v == 3:
                    plateau_jeu_suivant[i,j] = 1
                else:
                    plateau_jeu_suivant[i,j] = 0
            else:
                if nb_v == 3:
                    plateau_jeu_suivant[i,j] = 1
                else:
                    plateau_jeu_suivant[i,j] = 0
    return plateau_jeu_suivant


def animate(numero_iteration):
    global plateau_jeu
    x, y = [], []
    ##### Il faut mettre dans la liste x les abscisses des points à afficher, #####
    ##### et dans la liste y les ordonnées des points à afficher #####
    for i in range(1,N+1):
        for j in range(1,N+1):
            if plateau_jeu[i,j] == 1:
                x.append(j)
                y.append(i)

    #####
    ln.set_data(x, y) # Permet l'affichage des points (à ne pas modifier).
    plateau_jeu = etape_suivante_jeu_de_la_vie()
    plt.title(f'Itération n° {numero_iteration}')
    
    return ln, # Ne pas modifier.

fig = plt.figure()                # Initialise la figure.
ln, = plt.plot([], [], 'ro')      # Affichage vide (points rouges pour la suite).
N = 50                            # N représente la taille de la matrice.
vitesse = 4                       # La vitesse d'affichage.
plateau_jeu = np.zeros((N+2,N+2)) # Création de la matrice représentant le plateau de jeu.

#formes.croix(plateau_jeu,25,25)
#formes.grenouille(plateau_jeu,15,15)
formes.canon(plateau_jeu,10,5)
#formes.planeur(plateau_jeu,30,10)
#formes.clown(plateau_jeu, 32,20)

"""
formes.fleur(plateau_jeu, 20, 20)
formes.fleur(plateau_jeu, 40, 20)
formes.fleur(plateau_jeu, 20, 40)
formes.fleur(plateau_jeu, 40, 40)
formes.fleur(plateau_jeu, 30, 30)
"""
#formes.lwss(plateau_jeu, 20, 20)
#formes.bloc(plateau_jeu,20,20)

# Affichage de l'animation.
ani = anim.FuncAnimation(fig, animate, init_func=init, interval=100/vitesse)
plt.show() # Affiche la fenêtre - Pas nécessaire avec Spyder, mais l'est avec IDLE.
