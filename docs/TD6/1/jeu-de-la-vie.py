import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as anim
import formes

def init():
    """
    Initialise l'affichage de la fenêtre. Fonction à ne pas modifier !
    """
    plt.xlim(-1, N+1)
    plt.ylim(-1, N+1)
    return ln,

def nb_voisins_vivants(i, j):
    """
    Calcul le nombre de voisins vivants de la case (i, j).
    """
    ## TODO ##
    return 0

def etape_suivante_jeu_de_la_vie():
    """
    Génère une nouvelle matrice, de la même taille que plateau_jeu,
    qui représente l'état du plateau à l'étape suivante.
    """
    plateau_jeu_suivant = None
    ## TODO ##
    return plateau_jeu_suivant


def animate(numero_iteration):
    global plateau_jeu
    x, y = [], []
    ##### Il faut mettre dans la liste x les abscisses des points à afficher, #####
    ##### et dans la liste y les ordonnées des points à afficher #####
    
    ## TODO ##
    
    #####
    ln.set_data(x, y) # Permet l'affichage des points (à ne pas modifier).
    plateau_jeu = etape_suivante_jeu_de_la_vie()
    plt.title(f'Itération n° {numero_iteration}')
    
    return ln, # Ne pas modifier.

fig = plt.figure()                # Initialise la figure.
ln, = plt.plot([], [], 'ro')      # Affichage vide (points rouges pour la suite).
N = 50                            # N représente la taille de la matrice.
vitesse = 5                      # La vitesse d'affichage.
plateau_jeu = np.zeros((N+2,N+2)) # Création de la matrice représentant le plateau de jeu.

## Ajout des formes de départ.
## TODO ## Par exemple : formes.canon(plateau_jeu,10,5)


# Affichage de l'animation.
ani = anim.FuncAnimation(fig, animate, init_func=init, interval=100/vitesse)
plt.show() # Affiche la fenêtre - Pas nécessaire avec Spyder, mais l'est avec IDLE.