# Exercice 1 - 🧬 Le jeu de la vie

## 1. Le sujet

<center>
    [Sujet en PDF](TD4.pdf){ .md-button download="TD4.pdf" }
</center>

## 2. Les scripts à télécharger

<center>
    [exemple-animation-plot.py](exemple-animation-plot.py){ .md-button download="exemple-animation-plot.py" }
    [jeu-de-la-vie.py](jeu-de-la-vie.py){ .md-button download="jeu-de-la-vie.py" }
    [formes.py](formes.py){ .md-button download="formes.py" }
</center>