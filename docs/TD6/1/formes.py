def croix(m, i, j):
    """
    Permet de dessiner l'oscilateur "croix".
    
    Paramètres :
        - m : La matrice modélisant le plateau de jeu.
        - i : Le numero de la ligne du point le plus à gauche, et le plus en haut.
        - j : Le numero de la colonne du point le plus à gauche, et le plus en haut.
    """
    m[i,j]=1
    m[i,j+1]=1
    m[i,j+2]=1

def grenouille(m, i, j):
    """
    Permet de dessiner l'oscilateur "grenouille".
    Attention, il faut i>1.
    
    Paramètres :
        - m : La matrice modélisant le plateau de jeu.
        - i : Le numero de la ligne du point le plus à gauche, et le plus en haut.
        - j : Le numero de la colonne du point le plus à gauche, et le plus en haut.
    """
    m[i+1,j]=1
    m[i  ,j+3]=1
    m[i  ,j]=1
    m[i+1,j+3]=1
    m[i+2,j+1]=1
    m[i-1,j+2]=1

def planeur(m, i, j):
    """
    Permet de dessiner le glisseur "planeur".
    
    Paramètres :
        - m : La matrice modélisant le plateau de jeu.
        - i : Le numero de la ligne du point le plus à gauche, et le plus en haut.
        - j : Le numero de la colonne du point le plus à gauche, et le plus en haut.
    """
    m[i  ,j]=1
    m[i  ,j+2]=1
    m[i+1,j+1]=1
    m[i+1,j+2]=1
    m[i+2,j+1]=1

def clown(m, i, j):
    """
    Permet de dessiner le "U" qui donnera naissance au clown.
    
    Paramètres :
        - m : La matrice modélisant le plateau de jeu.
        - i : Le numero de la ligne du point le plus à gauche, et le plus en haut.
        - j : Le numero de la colonne du point le plus à gauche, et le plus en haut.
    """
    m[i  ,j]=1
    m[i  ,j+2]=1
    m[i+1,j]=1
    m[i+1,j+2]=1
    m[i+2,j]=1
    m[i+2,j+1]=1
    m[i+2,j+2]=1

def fleur(m, i, j):
    """
    Permet de dessiner la floraison.
    Attention, il faut j>1.
    
    Paramètres :
        - m : La matrice modélisant le plateau de jeu.
        - i : Le numero de la ligne du point le plus à gauche, et le plus en haut.
        - j : Le numero de la colonne du point le plus à gauche, et le plus en haut.
    """
    m[i  ,j]=1
    m[i  ,j+1]=1
    m[i  ,j+2]=1
    m[i+1,j-1]=1
    m[i+1,j+1]=1
    m[i+1,j+3]=1
    m[i+2,j]=1
    m[i+2,j+1]=1
    m[i+2,j+2]=1

def bloc(m, i, j):
    """
    Permet de dessiner un bloc de 4 cellules.
    
    Paramètres :
        - m : La matrice modélisant le plateau de jeu.
        - i : Le numero de la ligne du point le plus à gauche, et le plus en haut.
        - j : Le numero de la colonne du point le plus à gauche, et le plus en haut.
    """
    m[i  ,j]=1
    m[i  ,j+1]=1
    m[i+1,j]=1
    m[i+1,j+1]=1

def lwss(m, i, j):
    """
    Permet de dessiner le vaisseau LWSS.
    
    Paramètres :
        - m : La matrice modélisant le plateau de jeu.
        - i : Le numero de la ligne du point le plus à gauche, et le plus en haut.
        - j : Le numero de la colonne du point le plus à gauche, et le plus en haut.
    """
    m[i  ,j]=1
    m[i  ,j+3]=1
    m[i+1,j+4]=1
    m[i+2,j]=1
    m[i+2,j+4]=1
    m[i+3,j+1]=1
    m[i+3,j+2]=1
    m[i+3,j+3]=1
    m[i+3,j+4]=1

def canon(m, i, j):
    """
    Permet de dessiner le canon à planeurs.
    Attention, il faut i > 4.
    
    Paramètres :
        - m : La matrice modélisant le plateau de jeu.
        - i : Le numero de la ligne du point le plus à gauche, et le plus en haut.
        - j : Le numero de la colonne du point le plus à gauche, et le plus en haut.
    """
    m[i,1]=1
    m[i,j+1]=1
    m[i+1,j]=1
    m[i+1,j+1]=1


    m[i,j+10]=1
    m[i+1,j+10]=1
    m[i+2,j+10]=1
    m[i-1,j+11]=1
    m[i-2,j+12]=1
    m[i-2,j+13]=1
    m[i+3,j+11]=1
    m[i+4,j+12]=1
    m[i+4,j+13]=1

    m[i+1,j+14]=1

    m[i-1,j+15]=1
    m[i,j+16]=1
    m[i+1,j+16]=1
    m[i+1,j+17]=1
    m[i+2,j+16]=1
    m[i+3,j+15]=1


    m[i,j+20]=1
    m[i,j+21]=1
    m[i-1,j+20]=1
    m[i-1,j+21]=1
    m[i-2,j+20]=1
    m[i-2,j+21]=1
    m[i-3,j+22]=1
    m[i+1,j+22]=1

    m[i-3,j+24]=1
    m[i-4,j+24]=1
    m[i+1,j+24]=1
    m[i+2,j+24]=1


    m[i-2,j+34]=1
    m[i-1,j+34]=1
    m[i-2,j+35]=1
    m[i-1,j+35]=1
