# Exercice 2 - Pour aller plus loin 👉

Project Euler [https://projecteuler.net](https://projecteuler.net){ target=_blank } est un site web proposant des problèmes à résoudre à l'aide de programmes informatiques. Il contient beaucoup de problèmes qui sont classés par ordre de difficulté croissante.

Si vous êtes intéressés, n'hésitez pas à aller y jeter un oeil. Voici les 4 premiers exercices :

1) **Multiples de 3 ou 5.**

Si on liste les entiers naturels strictement inférieurs à 10 multiples de 3 ou de 5, on obtient 3, 5, 6 et 9. Leur somme donne 23.

Créer une fonction Python permettant de trouver la somme de tous les multiples de 3 ou de 5 strictement inférieurs à 1000.

2) **Plus grand produit palindromique.**

Un palindrome est un nombre qui se lit de la même façon dans les deux sens. Le plus grand palindrome qui est produit de deux nombres à deux chiffres est 9009 = 91×99.

Trouver le plus grand palindrome produit de deux nombres à trois chiffres.

3) **Termes paires de la suite de Fibonacci.**

La suite de Fibonacci est obtenu de la façon suivante :
le premier terme est 1, le deuxième est 2 et chaque terme est ensuite obtenu en additionnant les deux termes précédents. Les dix premiers termes sont les suivants : 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...

Trouver la somme des termes paires de la suite de Fibonacci en s'arrêtant quand un terme de la suite excède 4 millions.

4) **Plus grand facteurs premiers.**

Quel est le plus grand facteur premier dans la décomposition en facteurs premiers de 600 851 475 143 ?