def moyenne_note(nom_fichier):
    # On initialise la liste des notes (vide).
    notes = []
    with open(nom_fichier, 'r') as fichier:
        for note in fichier:
            # On ajoute chaque note contenue dans le fichier, après l'avoir transformée en float.
            notes.append(float(note))
    return sum(notes)/len(notes)

# Tests
assert round(moyenne_note('notes.txt'), 3) == 12.346

print(f"La moyenne est de {moyenne_note('notes.txt')}")