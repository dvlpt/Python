---
author: Benoît Domart
title: Exercice 2 - Manipuler le contenu d'un fichier
tags:
  - 1-boucle
  - 3-fichier
---
# Exercice 2 - Manipuler le contenu d'un fichier

Le fichier 📄`notes.txt` contient des notes obtenues par des étudiants. Chaque ligne du fichier ne contient qu'une seule note.

Compléter la fonction `moyenne_note` qui lit chaque ligne du fichier en paramètre, extrait les notes sous forme de `#!python float`, les stocke dans une liste, et affiche la moyenne de celles-ci.

<center>
    [notes.txt](notes.txt){ .md-button download="notes.txt" }
</center>

```py
def moyenne_note(nom_fichier):
    '''
    Renvoie la moyenne des notes contenues dans le fichier "nom_fichier".
    '''
    ...

# Tests
assert round(moyenne_note('notes.txt'), 3) == 12.346
```