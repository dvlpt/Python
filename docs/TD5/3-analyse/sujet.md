---
author: Benoît Domart
title: Exercice 3 - Analyser le contenu d'un fichier
tags:
  - 1-boucle
  - 3-fichier
  - 2-dictionnaire
---
# Exercice 3 - Analyser le contenu d'un fichier

Dans le fichier 📄`lorem-ipsum.txt`,

1. Quelle est la ligne la plus longue ?
2. Quel est le mot qui apparaît le plus ?

<center>
    [lorem-ipsum.txt](lorem-ipsum.txt){ .md-button download="lorem-ipsum.txt" }
</center>

???+warning "Attention !"

    Pour la seconde question,
    
    - Il ne faut pas prendre en compte les retours à la ligne (`\n`), les `!`, les `?`, les `,` et les `.`.
    - Il ne faut pas distinguer les majuscules et les minuscules (on peut tout mettre en minuscule par exemple).

???hint "Aide"

    On pourra, éventuellement,
    
    1. Parcourir toutes les lignes du fichier.
    2. Découper chaque ligne avec le séparateur ` ` (espace).
    3. Supprimer les caractères qui ne nous intéressent pas.
    4. Enfin, stocker dans un dictionnaire la liste des couples `(mot, nombre de fois où il apparaît)`.

???question "Rappels sur les dictionnaires"

    - On peut savoir si un dictionnaire contient déjà une clef ou pas.
        La condition `#!python 'lorem' in dico` permet par exemple de savoir si la clef `#!python 'lorem'` est déja présente ou non dans le dictionnaire `dico`.
    - Pour **ajouter** un couple `(clef, valeur)` dans un dictionnaire `dico`, ou **modifier** la valeur associée à une clef déjà existante, il faut utiliser l'instruction `#!python dico[clef] = valeur`.
        Par exemple, pour indiquer qu'il y a 5 `#!python 'lorem'`, on écrira `#!python dico['lorem'] = 5`.
    - On peut également incrémenter le nombre de `#!python 'lorem'` présents via `#!python dico['lorem'] = dico['lorem'] + 1` ou `#!python dico['lorem'] += 1`.