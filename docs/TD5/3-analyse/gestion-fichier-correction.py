def affiche_contenu_fichier(nom_fichier):
    """
    Affiche le contenu du fichier "nom_fichier", ligne par ligne.
    """
    with open(nom_fichier, 'r') as fichier:
        # L'objet File est itérable, on peut donc le parcourir avec un "for".
        for ligne in fichier:
            print(ligne)

def compte_nombre_mots(nom_fichier):
    """
    Renvoie le nombre de mots contenu dans le fichier "nom_fichier".
    """
    with open(nom_fichier, 'r') as fichier:
        # Nous allons créer une liste qui va contenir tous les mots.
        l = []
        for ligne in fichier:
            if ligne == '\n':
                # Si la ligne est uniquement un retour à la ligne,
                # on passe à la ligne suivante.
                continue
            # str.split permet de transformer une chaîne de caractères en list,
            # en indiquant le séparateur (ici c'est l'espace).
            l.extend(ligne.split(' '))
    return len(l)

# Tests
assert compte_nombre_mots('animaux.txt') == 3
assert compte_nombre_mots('lorem-ipsum.txt') == 668922