def affiche_contenu_fichier(nom_fichier):
    """
    Affiche le contenu du fichier "nom_fichier", ligne par ligne.
    """
    ...

def compte_nombre_mots(nom_fichier):
    """
    Renvoie le nombre de mots contenu dans le fichier "nom_fichier".
    """
    ...

# Tests
assert compte_nombre_mots(animaux.txt) == 3
assert compte_nombre_mots(lorem-ipsum.txt) == 668922