---
author: Benoît Domart
title: Exercice 1 - Lire le contenu d'un fichier
tags:
  - 1-boucle
  - 3-fichier
---
# Exercice 1 - Lire le contenu d'un fichier

## 1. Lecture

Compléter la fonction `affiche_contenu_fichier` qui prend en paramètre le **nom d'un fichier** (qui existe dans le même dossier que le script contenant la fonction), et affiche son contenu ligne par ligne.

```py
def affiche_contenu_fichier(nom_fichier):
    '''
    Affiche le contenu du fichier "nom_fichier", ligne par ligne.
    '''
    ...
```

## 2. Nombre de mots

Compléter la fonction `compte_nombre_mots` qui prend en paramètre le **nom d'un fichier** (qui existe dans le même dossier que le script contenant la fonction), et renvoie le nombre de mots contenus dans le fichier.

Attention, il ne faut pas compter les retours à la ligne tout seul (c'est-à-dire les mots "\n").

Vérifier par exemple que le fichier 📄`animaux.txt` contient 3 mots et que 📄`lorem-ipsum.txt` en contient 668 922.

<center>
    [animaux.txt](animaux.txt){ .md-button download="animaux.txt" }
    [lorem-ipsum.txt](lorem-ipsum.txt){ .md-button download="lorem-ipsum.txt" }
</center>

```py
def compte_nombre_mots(nom_fichier):
    '''
    Renvoie le nombre de mots contenu dans le fichier "nom_fichier".
    '''
    ...

# Tests
assert compte_nombre_mots('animaux.txt') == 3
assert compte_nombre_mots('lorem-ipsum.txt') == 668922
```