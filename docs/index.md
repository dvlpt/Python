# 🏡 Accueil

Exercices pour les TDs des 3A du DMS.

Beaucoup d'emprunts ont été effectués à [ce regroupement d'exercices pour NSI](https://e-nsi.gitlab.io/pratique/){target=_blank}

Ce site est sous licence <a
   href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1"
   target="_blank" rel="license noopener noreferrer"
   style="display:inline-block;">CC BY-NC-SA 4.0<img
   style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
   src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img
   style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
   src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img
   style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
   src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"><img
   style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
   src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a>