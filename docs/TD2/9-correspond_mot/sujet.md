---
author: Mireille Coilhac, Franck Chambon, Nicolas Revéret
title: Exercice 9 - Mots qui se correspondent
tags:
  - 2-string
  - 1-boucle
---
# Exercice 9 - Mots qui se correspondent

???+info "Mots à trou"
    - Un mot est ici une chaine de caractères composée uniquement de lettres de l'alphabet.
    - Un mot à trous comporte également zéro, une ou plusieurs fois le caractère `"."`.

    On dira que `mot_complet` correspond à `mot_a_trous`, si on peut remplacer chaque `"."` de `mot_a_trous` par une lettre de façon à obtenir `mot_complet`. 

    - `"INFO.MA.IQUE"` est un mot à trous,
    - `"INFORMATIQUE"` est un mot qui lui correspond,
    - `"AUTOMATIQUE"` est un mot qui ne lui correspond pas.

???+ abstract "Objectif"
    Écrire une fonction telle que `correspond(mot_complet, mot_a_trous)` renvoie un booléen qui détermine si « `mot_complet` correspond à `mot_a_trous` »


???+ example "Exemples"

    ```pycon
    >>> correspond("INFORMATIQUE", "INFO.MA.IQUE")
    True
    >>> correspond("AUTOMATIQUE", "INFO.MA.IQUE")
    False
    >>> correspond("INFO", "INFO.MA.IQUE")
    False
    >>> correspond("INFORMATIQUES", "INFO.MA.IQUE")
    False
    ```

{{ IDE('exo', affiche_correction=False) }}
