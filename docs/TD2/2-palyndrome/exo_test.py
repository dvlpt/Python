# Tests
assert renverser('informatique') == 'euqitamrofni'
assert renverser('python') == 'nohtyp'
assert renverser('') == ''

# Tests supplémentaires
assert renverser('abcde') == 'edcba'
assert renverser('159753') == '357951'
assert renverser('a6b-[]') == '][-b6a'
assert renverser('abc'*20) == 'cba'*20
