def est_palindrome(mot):
    ...


# Tests
assert not est_palindrome('informatique')
assert est_palindrome('kayak')
assert est_palindrome('')
assert est_palindrome('Engage le jeu que je le gagne') 