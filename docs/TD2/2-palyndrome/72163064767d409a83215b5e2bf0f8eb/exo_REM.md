# TD2 - Exercice 2 - Question 1

## Commentaires

### 1. 2 solutions

1. On crée tout d'abord une chaine vide destinée à contenir la chaine `mot` renversée. Ensuite, on ajoute chaque caractère de `mot` **au début** de `tom`.
2. On utilise le _slicing_ pour ne conserver qu'une lettre sur 2.

{{ IDE('exo_corr') }}

### 2. Variante avec les indices

On pouvait aussi parcourir les indices de la fin vers le début.

```python
def renverser(mot):
    tom = ''
    for i in range(len(mot)-1, -1, -1):
        tom = tom + mot[i]
    return tom
```

### 3. Variante fonctionnelle

Avec une liste par compréhension et on utilise `join` :

```python
def renverser(mot):
    return ''.join([mot[i] for i in range(len(mot)-1, -1, -1)])
```
