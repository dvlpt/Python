def est_palindrome(mot):
    # On met tout en minuscule.
    mot = mot.lower()
    # On supprime les espaces.
    mot = mot.replace(' ', '')
    return mot == renverser(mot)


# Tests
assert not est_palindrome('informatique')
assert est_palindrome('kayak')
assert est_palindrome('')
assert est_palindrome('Engage le jeu que je le gagne') 