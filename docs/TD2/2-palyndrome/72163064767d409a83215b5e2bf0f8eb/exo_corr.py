def renverser(mot):
    tom = ''
    for caractere in mot:
        tom = caractere + tom
    return tom
    
def renverser2(mot):
    return mot[::-1]


# Tests
assert not est_palindrome('informatique')
assert est_palindrome('kayak')
assert est_palindrome('')
assert est_palindrome('Engage le jeu que je le gagne') 