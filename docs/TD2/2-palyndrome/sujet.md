---
author: Benoît Domart
title: Exercice 2 - Palindrome
tags:
  - 2-string
---

# Exercice 2 - Palindrome

## 1.Renverser une chaine

Programmer une fonction `renverser` :

* prenant en paramètre une chaine de caractères `mot`,
  
* renvoyant une nouvelle chaine de caractères en inversant ceux de la chaine `mot`.


```pycon
>>> renverser('informatique')
'euqitamrofni'
>>> renverser('python')
'nohtyp'
>>> renverser('')
''
```


{{ IDE('exo', affiche_correction=False) }}

## 2. Palindrome

Compléter la fonction `est_palindrome`, qui utilise la fonction `renverse`, et qui renvoit un booléeen indiquant si la chaîne en paramètre est un palindrome.

???+note
    On rappelle qu'un palindrome est une chaîne de caractères se lisant de la même façon dans les deux sens, comme par exemple _kayak_.

    Attention, il ne faut pas tenir compte des espaces, ni des majuscules. Par exemple, "_Engage le jeu que je le gagne_" est également un palindrome.

{{ IDE('exo2', affiche_correction=False) }}
