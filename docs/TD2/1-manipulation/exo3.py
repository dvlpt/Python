def separe(mot):
    ...


# Tests

assert separe('informatique') == ['informatique']
assert separe('') == ['']
assert separe('Engage le jeu que je le gagne') == ['Engage', 'le', 'jeu', 'que', 'je', 'le', 'gagne']