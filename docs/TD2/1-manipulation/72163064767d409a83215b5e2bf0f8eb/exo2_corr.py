def moitie(mot):
    return mot[::2]


# Tests

assert moitie('informatique') == 'ifraiu'
assert moitie('kayak') == 'kyk'
assert moitie('') == ''
assert moitie('Engage le jeu que je le gagne') == 'Egg ejuqej egge'