def assemble(liste):
    return ' '.join(liste)


# Tests

assert assemble(['informatique']) == 'informatique'
assert assemble('') == ''
assert assemble(['Engage', 'le', 'jeu', 'que', 'je', 'le', 'gagne']) == 'Engage le jeu que je le gagne'