def derniere_lettre(mot):
    dernier = ''
    for caractere in mot:
        dernier = caractere
    return dernier
    
def derniere_lettre2(mot):
    if len(mot) <= 1:
        return mot
    return mot[-1]


# Tests
assert derniere_lettre('informatique') == 'e'
assert derniere_lettre('python') == 'n'
assert derniere_lettre('') == ''