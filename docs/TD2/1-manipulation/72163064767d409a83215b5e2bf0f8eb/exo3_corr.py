def separe(mot):
    # Il faut commencer par supprimer les espaces
    # qu'il peut y avoir en trop au début ou à la fin.
    mot = mot.strip()
    return mot.split(' ')


# Tests

assert separe('informatique') == ['informatique']
assert separe('') == ['']
assert separe('Engage le jeu que je le gagne') == ['Engage', 'le', 'jeu', 'que', 'je', 'le', 'gagne']
assert separe('avec un espace en trop à la fin ') == ['avec', 'un', 'espace', 'en', 'trop', 'à', 'la', 'fin']
assert separe(' avec un espace en trop au début') == ['avec', 'un', 'espace', 'en', 'trop', 'au', 'début']
assert separe(' avec 2 espaces en trop ') == ['avec', '2', 'espaces', 'en', 'trop']
assert separe('   espaces      ') == ['espaces']