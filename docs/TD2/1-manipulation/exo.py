def derniere_lettre(mot):
    ...


# Tests
assert derniere_lettre('informatique') == 'e'
assert derniere_lettre('python') == 'n'
assert derniere_lettre('') == ''