---
author: Benoît Domart
title: Exercice 1 - Manipulation de chaînes
tags:
  - 2-string
  - 2-liste
---

# Exercice 1 - Manipulation de chaînes

## 1.Dernière lettre

Écrire une fonction `derniere_lettre` qui renvoit la dernière lettre d'une chaîne de caractères.

{{ IDE('exo', affiche_correction=False) }}

## 2. Une lettre sur 2

Écrire une fonction `moitie` qui renvoit une lettre sur 2 d'une chaîne de caractères.

{{ IDE('exo2', affiche_correction=False) }}

## 3. Transformation en liste

Écrire une fonction `separe` qui sépare une chaîne de caractères par rapport aux espaces qu'elle contient.

{{ IDE('exo3', affiche_correction=False) }}

## 4. Retour en chaîne

Écrire une fonction `assemble` qui fait le contraire de `separe`, c'est-à-dire que :

- pour toute chaîne de caractères `mot` ne contenant pas d'espaces au début ou à la fin, `assemble(separe(mot)) == mot`,
- pour toute liste contenant des chaînes de caractères `liste`, `separe(assemble(liste)) == liste`,

{{ IDE('exo4', affiche_correction=False) }}