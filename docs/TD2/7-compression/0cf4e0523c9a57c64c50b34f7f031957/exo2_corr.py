def compresse(chaine):
    result = ''
    prec = chaine[0]
    cpt = 0
    for car in chaine:
        if car == prec:
            cpt += 1
        else:
            while cpt >= 10:
                result += str(9) + prec
                cpt -= 9
            result += str(cpt) + prec
            cpt = 1
        prec = car
    while cpt >= 10:
        result += str(9) + prec
        cpt -= 9
    result += str(cpt) + prec
    return result

# Tests

assert compresse('aaacaa9999') == '3a1c2a49'
assert compresse('aaaaa') == '5a'
assert compresse('33322233') == '333223'