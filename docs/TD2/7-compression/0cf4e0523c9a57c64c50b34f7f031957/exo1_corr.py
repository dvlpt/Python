def decompresse(chaine):
    result = ''
    for i in range(0, len(chaine), 2):
        result += int(chaine[i]) * chaine[i+1]
    return result

# Tests

assert decompresse('3a0b1c2a49') == 'aaacaa9999'
assert decompresse('5a') == 'aaaaa'
assert decompresse('333223') == '33322233'