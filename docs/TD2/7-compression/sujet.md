---
author: Benoît Domart
title: Exercice 7 - Compression et décompression
tags:
    - 1-boucle
    - 2-string
---

# Exercice 7 - Compression et décompression

## 1. Décompression

Compléter la fonction `decompresse` qui prend en paramètre une chaîne de caractères, et qui la décompresse selon les règles suivantes :

- la longueur du paramètre `chaine` est paire (et non nulle).
- tous les caractères de `chaine` d'indice pair sont des chiffres (entre 0 et 9).
- le résultat est calculé en répétant chaque caractère d'indice impair de `chaine` le nombre de fois indiqué par le chiffre juste avant. 

{{ IDE('exo1') }}


## 2. Compression

Compléter la fonction `compresse` qui prend en paramètre une chaîne de caractères, et qui la compresse selon les règles inverses de ci-dessus :

{{ IDE('exo2', affiche_correction=False) }}
