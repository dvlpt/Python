# Tests

assert decompresse('3a0b1c2a49') == 'aaacaa9999'
assert decompresse('5a') == 'aaaaa'
assert decompresse('333223') == '33322233'

# Autres tests

assert decompresse('9a3a') == 'aaaaaaaaaaaa'