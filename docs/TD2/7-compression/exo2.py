def compresse(chaine):
    ...

# Tests

assert compresse('aaacaa9999') == '3a1c2a49'
assert compresse('aaaaa') == '5a'
assert compresse('33322233') == '333223'