---
author: Benoît Domart
title: Exercice 8 - Dictionnaire des occurences
tags:
    - 1-boucle
    - 2-dictionnaire
    - 2-string
---

# Exercice 8 - Dictionnaire des occurences

Compléter le programme suivant.

La fonction `compte_caracteres` prend en paramètre un texte (une chaîne de caractères) contenant uniquement des minuscules sans accents et des espaces.

Elle renvoie un dictionnaire dont les 27 clefs sont les 26 lettres de `a` à `z` et le caractère _espace_. Les valeurs sont le nombre de fois où le caractère de la clef apparaît (0 par défaut).

???+note "Rappel"
    Pour ajouter un couple clef/valeur dans un dictionnaire, ou modifier la valeur associée à une clef déjà existante, il faut utiliser l'instruction `dico[clef] = valeur`.
	Par exemple, pour indiquer qu'il y a 5 `r`, on écrira `resultat['r'] = 5`.

{{ IDE('exo', affiche_correction=False) }}
