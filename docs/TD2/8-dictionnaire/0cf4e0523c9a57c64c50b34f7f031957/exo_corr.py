def compte_caracteres(texte):
    """
    Paramètre : texte - Une chaîne de caractère ne contenant que des minuscules sans accent et des espaces.
    Sortie : resultat - Un dictionnaire dont les clefs sont les 27 caractères possibles et les valeurs le nombre de fois où ils apparaissent dans "texte".
    """
    # Le résultat est un dictionnaire, où les clefs sont tous les caractères
    # possibles, et les valeurs sont le nombre de fois où il apparaît dans texte.
    resultat = {}
    # La liste des 27 caractères à dénombrer.
    caracteres = 'abcdefghijklmnopqrstuvwxyz '
    
    # Remarque : On pourrait faire les deux boucles for imbriquées dans l'autre sens.
    for c in caracteres:
        # On parcourt les 27 caractères.
        # Pour chacun, on initialise un compteur à 0.
        cpt = 0
        for lettre in texte:
            # On parcourt chaque lettre du texte en paramètre de la fonction.
            if lettre == c:
                cpt += 1
        resultat[c] = cpt
        
    return resultat
    
def compte_caracteres_solution2(texte):
    """
    Paramètre : texte - Une chaîne de caractère ne contenant que des minuscules sans accent et des espaces.
    Sortie : resultat - Un dictionnaire dont les clefs sont les 27 caractères possibles et les valeurs le nombre de fois où ils apparaissent dans "texte".
    """
    # Le résultat est un dictionnaire, où les clefs sont tous les caractères
    # possibles, et les valeurs sont le nombre de fois où il apparaît dans texte.
    resultat = {}
    # La liste des 27 caractères à dénombrer.
    caracteres = 'abcdefghijklmnopqrstuvwxyz '
    
    for c in caracteres:
        # On parcourt les 27 caractères. Pour chacun, on initialise un compteur à 0. C'est la phase d'initialisation.
        resultat[c] = 0
        
    for lettre in texte:
        # On parcourt ensuite chaque lettre du texte et on incrémente la valeur de la clef correspondant à la lettre courante.
        resultat[lettre] += 1
        
    return resultat

# Tests

assert compte_caracteres('a') == {'a':1,'b':0,'c':0,'d':0,'e':0,'f':0,'g':0,'h':0,'i':0,'j':0,'k':0,'l':0,'m':0,'n':0,'o':0,'p':0,'q':0,'r':0,'s':0,'t':0,'u':0,'v':0,'w':0,'x':0,'y':0,'z':0,' ':0}
assert compte_caracteres('r') == {'a':0,'b':0,'c':0,'d':0,'e':0,'f':0,'g':0,'h':0,'i':0,'j':0,'k':0,'l':0,'m':0,'n':0,'o':0,'p':0,'q':0,'r':1,'s':0,'t':0,'u':0,'v':0,'w':0,'x':0,'y':0,'z':0,' ':0}
assert compte_caracteres('hello world') == {'a':0,'b':0,'c':0,'d':1,'e':1,'f':0,'g':0,'h':1,'i':0,'j':0,'k':0,'l':3,'m':0,'n':0,'o':2,'p':0,'q':0,'r':1,'s':0,'t':0,'u':0,'v':0,'w':1,'x':0,'y':0,'z':0,' ':1}
assert compte_caracteres('s a l u t') == {'a':1,'b':0,'c':0,'d':0,'e':0,'f':0,'g':0,'h':0,'i':0,'j':0,'k':0,'l':1,'m':0,'n':0,'o':0,'p':0,'q':0,'r':0,'s':1,'t':1,'u':1,'v':0,'w':0,'x':0,'y':0,'z':0,' ':4}