def bien_parenthesee(expression):
    ...

# Test

assert bien_parenthesee('abc')
assert bien_parenthesee('(abc)')
assert bien_parenthesee('ab(cd)ef')
assert bien_parenthesee('a(b)c(d)e')
assert bien_parenthesee('a((b)c)d')
assert bien_parenthesee('a(b(c()e)f)g')
assert not bien_parenthesee('(')
assert not bien_parenthesee(')')
assert not bien_parenthesee('abc)')
assert not bien_parenthesee('ab)c')
assert not bien_parenthesee('a(b(c)d')
assert not bien_parenthesee('a(b)c)d(e)f')