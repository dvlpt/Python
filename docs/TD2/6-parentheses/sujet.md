---
author: Benoît Domart
title: Exercice 6 - Expression bien parenthésée (1)
tags:
    - 1-boucle
    - 2-string
---

# Exercice 6 - Expression bien parenthésée (1)

Écrire une fonction Python `bien_parenthesee` qui prend en paramètre une expression, sous forme de chaine de caractères, et qui renvoie un booléen indiquant si elle est bien parenthésée. Pour cela, il faut vérifier que toute parenthèse ouverte est ensuite fermée, et une parenthèse ne peut pas être fermée si elle n'a pas été préalablement ouverte. Le tableau ci-dessous donne des exemples de chaînes bien et mal parenthésées :

| Bien parenthésées | Mal parenthésées |
| ----------------- | ---------------- |
| `'abc'`           | `'('`            |
| `'(abc)'`         | `')'}`           |
| `'ab(cd)ef'`      | `'abc)'`         |
| `'a(b)c(d)e'`     | `'ab)c'`         |
| `'a((b)c)d'`      | `'a(b(c)d'`      |
| `'a(b(c()e)f)g'`  | `'a(b)c)d(e)f'`  |

{{ IDE('exo', affiche_correction=False) }}
