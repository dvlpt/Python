def bien_parenthesee(expression):
    cpt = 0
    for lettre in expression:
        if lettre == '(':
            cpt += 1
        if lettre == ')':
            cpt -= 1
        if cpt < 0:
            return False
    return cpt == 0

# Autre façon d'écrire la même chose :
def bien_parenthesee2(expression):
    cpt_gauche = 0
    cpt_droite = 0
    for car in expression:
        if car == '(':
            cpt_gauche += 1
        elif car == ')':
            cpt_droite += 1
        if cpt_droite > cpt_gauche:
            return False
    return cpt_gauche == cpt_droite

# Test

assert bien_parenthesee('abc')
assert bien_parenthesee('(abc)')
assert bien_parenthesee('ab(cd)ef')
assert bien_parenthesee('a(b)c(d)e')
assert bien_parenthesee('a((b)c)d')
assert bien_parenthesee('a(b(c()e)f)g')
assert not bien_parenthesee('(')
assert not bien_parenthesee(')')
assert not bien_parenthesee('abc)')
assert not bien_parenthesee('ab)c')
assert not bien_parenthesee('a(b(c)d')
assert not bien_parenthesee('a(b)c)d(e)f')