# TD1 - Exercice 6

## Commentaires

On compte le nombre de parenthèses gauche et droite.

À aucun moment, il ne peut y avoir plus de parenthèses droites que de gauches.

À la fin, il faut qu'il y en ait le même nombre.

{{ IDE('exo_corr') }}