#!/bin/sh

warning="**Avertissement :**"
error="**Erreur :**"
item="*"


# Pour ne garder que ce qui se trouve entre 2 lignes données
# Cela sert pour garder l'intérieur des blocs HDR
garder_entre () {
    fich="$1"
    while [ $# -gt 2 ]
    do
            #echo $1 $2
            debut=$(($2+1))
            fin=$(($3-1))
            sed -n "$debut,${fin}p" "$fich"
            shift 2
    done 
}


# Pour tester tous les exos d'un niveau
verif_niveau () {
    niveau=$1
    nom_niveau=$(basename `realpath $niveau`)
    echo ""
    echo "# Vérification de \`$nom_niveau\`"
    echo ""
    erreur=0
    for dossier in `ls $niveau`
    do
        if [ -d "$niveau/$dossier" ]
        then
            verif_exercice "$niveau/$dossier"
            if [ $? -ne 0 ]
            then
                erreur=1
            fi
        fi
    done
    return $erreur
}

# Pour tester un exercice en particulier
# Le premier paramètre, c'est le dossier
# En mode VERBOSE,
# on affiche les erreurs Python
# et un message si tout va bien
verif_exercice () {
    dossier=$1
    if [ -d "$dossier" ]
    then
        ok=1
        nom_exercice=$(basename `realpath $dossier`)
        resultat_affichage=""
        # on regarde s'il y a un dossier de hashage
        dossier_secret="$dossier"
        if [ -f "$dossier/clef.txt" ]
        then
            fichier_clef="$dossier/clef.txt"
            hash=$(echo -e "e-nsi+$(cat ${fichier_clef})" | md5sum | sed s/\ .*//)
            dossier_secret="$dossier/$hash"
            echo "$dossier_secret"
        fi
        if [ ! -f $dossier_secret/exo_corr.py ]
        then
            ok=0
            #echo -e "$warning Pas de correction"
            resultat_affichage="$resultat_affichage* $warning Pas de \`exo_corr.py\`\n"
        fi
        if [ ! -f $dossier_secret/exo_REM.md ]
        then
            ok=0
            #echo -e "$warning Pas de \`exo_REM.md\` pour \`$nom_exercice\`"
            resultat_affichage="$resultat_affichage* $warning Pas de \`exo_REM.md\`\n"
            if [ $REPARE -eq 0 ]
            then
                 resultat_affichage="$resultat_affichage Réparation : \`printf \"# Commentaires\\\\n\\\\n{{ IDE(\'exo_corr\') }}\\\\n\" > \"$dossier_secret/exo_REM.md\"\`\n"
            else
                 printf "# Commentaires\n\n{{ IDE('exo_corr') }}\n" > "$dossier_secret/exo_REM.md"
                 resultat_affichage="$resultat_affichage C'est corrigé !\n"
                 git add "$dossier_secret/exo_REM.md"            
            fi
        else
                # on regarde s'il y a l'IDE
                grep -qe "{{[ ]*IDE('exo_corr')[ ]*}}" "$dossier_secret/exo_REM.md"
                if [ $? -ne 0 ]
                then
                        #echo -e "$warning Mauvaise syntaxe IDE"
                        resultat_affichage="$resultat_affichage* $warning Pas de IDE ou mauvaise syntaxe dans \`exo_REM.md\`\n" 
                        ok=0
                        if [ $REPARE -eq 0 ]
                        then
                        resultat_affichage="$resultat_affichage Réparation : \`sed -i \"1i # Commentaires\\\\n\\\\n{{ IDE(\'exo_corr\') }}\" $dossier_secret/exo_REM.md\`\n"
                        else
                           sed -i "1i # Commentaires\n\n{{ IDE('exo_corr') }}" "$dossier_secret/exo_REM.md"
                           git add "$dossier_secret/exo_REM.md"
                           resultat_affichage="$resultat_affichage C'est corrigé !\n"   
                        fi
                fi

        fi
        # On regarde s'il y a au moins un fichier de test
        au_moins_un_test=0
        for fichier_test in $dossier/exo*_test.py
        do
                if [ -f  "$fichier_test" ]
                then
                    au_moins_un_test=1
                fi
        done
        if [ $au_moins_un_test -eq 0 ]
        then
            ok=0
            #echo -e "$warning Pas de tests"
            resultat_affichage="$resultat_affichage* $warning Pas de \`exo_test.py\`\n"
        fi
        if [ $ok -eq 1 ]
        then
            # On va vérifier que les corrections passent les tests
            # On récupère la partie HDR s'il y en a une :
            # Il faudrait le faire pour chaque exo.py ?
            grep -qe "---[- ]*HDR[- ]*---" "$dossier/exo.py"
            if [ $? -ne 0 ]
            then
                    # pas de HDR
                    touch test1.py
            else
                    num_lignes=`grep -ne "---[- ]*HDR[- ]*---" "$dossier/exo.py" | sed 's/:.*//'`
                    garder_entre "$dossier/exo.py" $num_lignes >> test1.py
            fi
            cat $dossier_secret/exo_corr.py $dossier/exo*_test.py >> test1.py
            #cat test1.py
            if [ ! -z $VERBOSE ]
            then
                    timeout 5 python3 test1.py
            else
                    timeout 5 python3 test1.py 2>&-
            fi
            case $? in
                    0)
                    #        if [ ! -z $VERBOSE ]
                    #        then
                    #            echo -e "Tout va bien pour \033[0;32m$dossier\033[0m"
                    #        fi
                            ;;
                    124)
                            #echo -e "$error Timeout pour le test de la correction"
                            resultat_affichage="$resultat_affichage* $error Timeout pour le test de la correction\n"
                            ok=0
                            ;;
                    *)
                            #echo -e "$error La correction ne fonctionne pas"
                            resultat_affichage="$resultat_affichage* $error La correction ne fonctionne pas\n"
                            ok=0
                            ;;
            esac
            rm test1.py
        fi
        if [ -f "$dossier/sujet.md" ]
        then
                # on vérifie pour la syntaxe d'IDE
                grep -qe "{{[ ]*IDE('.*'.*)[ ]*}}" "$dossier/sujet.md"
                if [ $? -ne 0 ]
                then
                        #echo -e "$warning Mauvaise syntaxe IDE"
                        resultat_affichage="$resultat_affichage* $warning Mauvaise syntaxe IDE\n"                       
                        ok=0
                fi
                # on regarde s'il n'y a pas tag: à la place de tags:
                grep -q '^tag:' "$dossier/sujet.md"
                if [ $? -eq 0 ]
                then
                        #echo -e "$warning Il y a \`tag:\` au lieu de \`tags:\` dans \`sujet.md\` ; Correction possible avec \"sed -i \'s/^tag:/tags:/\' $dossier/sujet.md\""
                        resultat_affichage="$resultat_affichage* $warning Il y a \`tag:\` au lieu de \`tags:\` dans \`sujet.md\` ; Correction possible avec \`sed -i \'s/^tag:/tags:/\' $dossier/sujet.md\`\n"                                          
                        ok=0 # peut être pas nécessaire
                fi
                # on regarde si ça commence bien par ---
                premiere=$(head -1 $dossier/sujet.md)
                if [ "$premiere" != "---" ]  # rajouter une expression régulière en cas d'espaces à la fin de la ligne ?
                then
                        #echo -e "$warning pas --- au début de \`sujet.md\` ; Correction possible avec \`\"sed -i \'1s/^/---\n/\' $dossier/sujet.md\`\""
                        resultat_affichage="$resultat_affichage* $warning pas --- au début de \`sujet.md\` ; Correction possible avec \`sed -i \'1s/^/---\\\\n/\' $dossier/sujet.md\`\n"                                                                        
                        ok=0 # peut être pas nécessaire
                fi
        else
                #echo -e "$warning pas de fichier \`sujet.md\` pour \`$nom_exercice\`"
                resultat_affichage="$resultat_affichage* $warning pas de fichier \`sujet.md\`\n"                                                                                        
                ok=0
        fi
        au_moins_un_exo=0
        # on sait qu'il y a au moins un de chaque
        #exos="$dossier/exo*([^_]).py"  # version qui ne marche pas, c'est triste
        # du coup, on récupere tous les fichiers python et on vire ceux avec des _
        exos=$(echo $dossier/exo*.py | sed 's/[^ ]*_[^\/ _]*.py[ ]*//g' | sed 's/^ //')
        for fichier_exo in $exos
        do
                #echo $fichier_exo
                if [ -f  "$fichier_exo" ]
                then
                    au_moins_un_exo=1
                fi
        done
        if [ $au_moins_un_exo -eq 0 ]
        then
            ok=0
            echo -e "Pas de \`exo.py\`"
        fi
        # maintenant on verifie la cohérence entre exo et exo_test
        if [ $ok -eq 1 ]
        then
                # on sait qu'il y a au moins un de chaque
                #exos="$dossier/exo*([^_]).py"  # version qui ne marche pas, c'est triste
                #exos=$(echo $dossier/exo*.py | sed 's/[^ _]*_[^ _]*.py[ ]*//g' | sed 's/^ //')
                tests="$dossier/exo*_test.py"
                prefixes_exos=$(echo $exos | sed 's/\.py//g')
                prefixes_tests=$(echo $tests | sed 's/_test\.py//g')
                if [ ! "$prefixes_exos" = "$prefixes_tests" ]
                then
                        #echo -e "Incohérence de nom entre exo*.py et exo*_test.py \033[0;34m$dossier\033[0m"
                        resultat_affichage="$resultat_affichage* $error Incohérence de nom entre \`exo*.py\` et \`exo*_test.py\`\n"                                                                                     
                        ok=0
                else
                        # maintenant on va vérifier que les énoncés seuls ne suffisent pas à passer les tests
                        # on vire les fichiers qui ne contiennent pas de assert
                        for fichier_test in `grep -l assert $tests`
                        do
                                # On teste le fichier de test tout seul, au cas où
                                if [ ! -z $VERBOSE ]
                                then
                                    timeout 5 python3 $fichier_test
                                else
                                    timeout 5 python3 $fichier_test 2>&-
                                fi
                                if [ $? -eq 0 ]
                                then
                                        #echo -e "Le fichier de test \033[0;31m$fichier_test\033[0m contient peut-être la correction"
                                        resultat_affichage="$resultat_affichage* $warning \`exo_test.py\` contient peut-être la correction\n"
                                        ok=0
                                else
                                        # On rajoute les énoncés, au cas où
                                        cat $exos $fichier_test > test1.py
                                        if [ ! -z $VERBOSE ]
                                        then
                                            timeout 5 python3 test1.py
                                        else
                                            timeout 5 python3 test1.py 2>&-
                                        fi
                                        if [ $? -eq 0 ]
                                        then
                                                #echo -e "Il y a peut-être un corrigé dans un fichier élève de \033[0;31m$dossier\033[0m"
                                                resultat_affichage="$resultat_affichage* $warning Il y a peut-être un corrigé dans \`exo.py\`\n"
                                                ok=0
                                        fi
                                        rm test1.py
                                fi
                        done

                fi
        fi
        if [ $ok -eq 0 ]
        then
                echo "## $nom_exercice"
                printf "$resultat_affichage"
        fi
        if [ $ok -eq 1 ] &&  [ ! -z $VERBOSE ]
        then
                echo -e "Tout va bien pour \033[0;32m$dossier\033[0m"
        fi
        if [ $ok -eq 1 ]
        then
                return 0
        else
                return 1
        fi
    else
        echo "le dossier $dossier n'existe pas"
        return 1
    fi
}


# Fonction qui permet de ne tester que la partie IDE
# Ce n'est plus nécessaire
verif_ide () {
   echo "******************************"
   echo "Vérification des IDE"
   echo "******************************"
   for sujet in $(grep -L -e "{{[ ]*IDE('.*')[ ]*}}" N?/*/sujet.md)
   do
           echo -e "\033[0;31m$sujet\033[0m n'a pas d'IDE valide"
   done
}


# Pour savoir si on veut tester un niveau ou un problème
gerer_param () {
    if [ -d $1 ]
    then
        if [ -f $1/sujet.md ]
        then
                verif_exercice "$1" verbeux
                #erreur=$?
        else
                verif_niveau "$1"
                #erreur=$?
        fi
        return $?
    else
        echo "Il n'y a pas de dossier $1"
        return 1
    fi
}


VERBOSE=""
REPARE=0

while getopts vr option
do
 case $option in
  v) #affiche les messages d'erreurs
    VERBOSE="verbose"
    ;;
  r) #repare ce qui peut l'être
    REPARE=1
   ;;
 esac
done

shift $((OPTIND-1)) 


if [ $# = 0 ]
then
    echo "Permet de vérifier :"
    echo "- qu'il y a un fichier sujet.md"
    echo "- qu'il contient bien {{ IDE('...') }}"
    echo "- qu'il y a au moins un fichier de test exo*_test.py"
    echo "- qu'il y a un fichier de correction exo_corr.py"
    echo "- que tous les tests sont validés par la correction"
    echo "Usage : verif_niveau.sh DOSSIER [DOSSIER]*"
    echo "DOSSIER est soit un niveau (N1, N2, N3) ou un dossier d'exercice (N1/mon_exo)"
    exit 1
else
    erreur=0
    while [ $# -gt 0 ]
    do
        gerer_param "$1"
        if [ $? -ne 0 ]
        then
                erreur=1
        fi
        shift
    done
    exit $erreur
fi

