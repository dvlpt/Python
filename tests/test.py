# coding: utf-8
import hashlib
import selenium
import selenium.webdriver
from selenium.webdriver.remote.webelement import By
from selenium.webdriver.common.keys import Keys
import sys

BASE_URL = "https://e-nsi.gitlab.io/DMS3A"
BASE_DIR = "./docs"

arg = sys.argv[1]


def get_secret():
    with open(f"{BASE_DIR}/{arg}/clef.txt", "r", encoding="utf-8") as f:
        clef = f.read()
    return hashlib.md5(("e-nsi+" + clef).encode("utf8")).hexdigest()


secret = get_secret()

options = selenium.webdriver.FirefoxOptions()
options.headless = True
browser = selenium.webdriver.Firefox(options=options)


browser.get(f"{BASE_URL}/{arg}/sujet/")
with open(f"{BASE_DIR}/{arg}/{secret}/exo_corr.py", "r", encoding="utf-8") as fp:
    correction = fp.read()

champ_reponse = browser.find_element(by=By.TAG_NAME, value="textarea")
parent = champ_reponse.find_element(by=By.XPATH, value="./..")
num = parent.get_attribute("id").split("_")[1]

champ_reponse.send_keys(Keys.CONTROL + "a")
champ_reponse.send_keys(Keys.DELETE)
for ligne in correction.split("\n"):
    champ_reponse.send_keys(ligne)
    champ_reponse.send_keys(Keys.HOME)
browser.execute_script(f'interpretACE("editor_{num}","h");')
browser.execute_script(f'executeTest("{num}","h");')
browser.close()
